# Paragraphs auto anchor

## Description
Provides an empty anchor link for every paragraph
based on its UUID. For example:
```
<a name="93f89463-cfc1-4d59-9a7b-be447a632d75"></a>
```
Then on the edit interface for a paragraph widget there is a button to copy the
UUID for the editor to create anchors.

## Requirements

* Paragraphs module.

## Configuration

Render you paragraphs as usual and the use anchor links to
scroll directly to that paragraph. For example:
```
https://example.com/my-page-with-paragraphs#93f89463-cfc1-4d59-9a7b-be447a632d75
```

## Caveats

The anchor link for each paragraph is injected into the `content`
variable in the twig template so be sure that part is render just like the
default paragraph template.
