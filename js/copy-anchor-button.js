/**
 * @file
 * Paragraphs actions JS code for paragraphs auto anchor.
 */

(function (Drupal, once) {

  'use strict';

  const unsecuredCopyToClipboard = function (text) {
    const textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();
    try {
      document.execCommand('copy');
    } catch (err) {
      console.error('Unable to copy to clipboard', err);
    }
    document.body.removeChild(textArea);
  }

  /**
   * Handle event when "Copy anchor" button is clicked.
   *
   * @param event
   *   Click event.
   */
  const clickHandler = function (event) {
    event.preventDefault();
    event.stopPropagation();
    // Prepend a `#` to the UUID.
    const anchor = `#${event.target.dataset.drupalParagraphsAutoAnchorCopy}`;
    // When in https the clipboard API is available.
    if (navigator.clipboard) {
      navigator.clipboard.writeText(anchor);
    // Otherwise fallback to the deprecated method.
    } else {
      unsecuredCopyToClipboard(anchor);
    }
    // Close menu.
    event.currentTarget.parentElement.parentElement.classList.remove('open');

    return false;
  };

  /**
   * Process paragraph auto anchor elements.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.paragraphCopyAnchorButton = {
    attach: function (context, settings) {
      const elements = once('paragraphs-copy-anchor-button', '[data-drupal-paragraphs-auto-anchor-copy]', context);
      elements.forEach(el => {
        el.addEventListener('click', clickHandler);
      });
    }
  };

})(Drupal, once);
